import axios, { AxiosInstance, AxiosResponse } from "axios";
import dotenv from "dotenv";
import { formatQuotes, formatConversion } from "../services/normalize";
import { StatusError } from "../types/kryptoTypes";

dotenv.config();

const apiCode = process.env.API_KEY;

export default class KryptoKen {
  baseUrl: string = "https://pro-api.coinmarketcap.com/";
  axiosInstance: AxiosInstance;

  constructor() {
    this.axiosInstance = axios.create({
      baseURL: this.baseUrl,
    });
  }

  async quotes(btc: string | Array<string>) {
    try {
      const requestUrl = `v1/cryptocurrency/quotes/latest?symbol=${btc}&CMC_PRO_API_KEY=${apiCode}`;
      const response: AxiosResponse<any, any> = await this.axiosInstance.get(
        requestUrl
      );

      const bitcoinInfo = formatQuotes(response.data);

      return bitcoinInfo;
    } catch (e) {
      if (axios.isAxiosError(e)) {
        return e.response?.data as StatusError;
      }
      return undefined;
    }
  }

  async conversion(quantity: number, btc: string, convert: Array<string>) {
    try {
      const requestUrl = `v1/tools/price-conversion?symbol=${btc}&amount=${quantity}&convert=${convert}&CMC_PRO_API_KEY=${apiCode}`;
      const response: AxiosResponse<any, any> = await this.axiosInstance.get(
        requestUrl
      );

      const convertInfo = formatConversion(response.data);

      return convertInfo;
    } catch (e) {
      if (axios.isAxiosError(e)) {
        return e.response?.data as StatusError;
      }
      return undefined;
    }
  }
}
