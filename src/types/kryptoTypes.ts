interface USD {
  price: number;
  last_updated: Date;
}

interface Quote {
  USD: USD;
}

export interface BTC {
  id: number;
  name: string;
  symbol: string;
  slug: string;
  date_added: Date;
  last_updated: Date;
  quote: Quote;
}

export interface Convert {
  id: number;
  symbol: string;
  name: string;
  amount: number;
  last_updated: Date;
  quote: ETH;
}

interface ETH {
  price: number;
  last_updated: Date;
}

export interface Error400 {
  status: StatusError;
}

export interface StatusError {
  timestamp: Date;
  error_code: number;
  error_message: string;
  elapsed: number;
  credit_count: number;
  notice: null;
}

export function isError400(error: any): error is Error400 {
  return (error as Error400).status !== undefined;
}

export function isBTC(btcInfo: any): btcInfo is BTC {
  return (btcInfo as BTC).id !== undefined;
}

export function isConvert(dataConvertion: any): dataConvertion is Convert {
  return (dataConvertion as Convert).id !== undefined;
}
