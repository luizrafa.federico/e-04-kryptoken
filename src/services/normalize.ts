import { BTC, Convert } from "../types/kryptoTypes";
import { AxiosResponse } from "axios";

export const formatQuotes = (data: AxiosResponse) => {
  const btcInfo = data.data.BTC;

  const { id, name, symbol, slug, date_added, last_updated, quote }: BTC =
    btcInfo;

  const output = {
    data: {
      BTC: {
        id: id,
        name: name,
        symbol: symbol,
        slug: slug,
        date_added: date_added,
        last_updated: last_updated,
        quote: {
          USD: {
            price: quote.USD["price"],
            last_updated: quote.USD["last_updated"],
          },
        },
      },
    },
  };

  return output;
};

export const formatConversion = (data: AxiosResponse) => {
  const dataConvertion = data.data;

  const { id, symbol, name, amount, last_updated, quote }: Convert =
    dataConvertion;

  const output = {
    data: {
      id: id,
      symbol: symbol,
      name: name,
      amount: amount,
      last_updated: last_updated,
      quote: {
        ETH: {
          price: quote.price,
          last_updated: quote.last_updated,
        },
      },
    },
  };

  return output;
};
